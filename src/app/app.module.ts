import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PagesModule} from './pages/pages.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {CommonServicesService} from './pages/services/common-services.service';
import {CommonConstantService} from '../app/pages/constants/common-constants'


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    PagesModule, HttpClientModule,
    FormsModule, ReactiveFormsModule,
    BrowserAnimationsModule,

  ],
  providers: [CommonServicesService, CommonConstantService],
  bootstrap: [AppComponent]
})
export class AppModule { }
