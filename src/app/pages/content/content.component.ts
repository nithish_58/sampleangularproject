import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { CommonServicesService } from '../services/common-services.service';
import {CommonConstantService} from '../../pages/constants/common-constants';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  //getting data from parent
  @Input('fromparent') name:string;
  
  // sending data to parent

  @Output() childToParent = new EventEmitter<String>();

  // Arrays declared to store imagelist
  public thumbNailImagesList: any = [];
  // public largerImagesList: any = [];
  public currentIndex = 0;
  // setting start and end index
  public thumbnailOffsetIndex: number = 0;
  public thumbnailEndIndex: number = 3;

  public thumbNailSnap: any = [];
  // images path 
  public imagePrefix = '../assets/images/thumbnails/';
  public mainImagePrefix = '../assets/images/large/';
  public selectedImage: any = {};

  // enable/disable flags
  public leftDisabled = true;
  public rightDisabled = true;

  constructor(private commonService: CommonServicesService, private commonConstants: CommonConstantService) {
   }

  ngOnInit() {
    this.getThumbNailImages();
  }

  getThumbNailImages() {
    this.commonService.getThumbNailImages().subscribe((data) => {
      this.thumbNailImagesList = data;
      console.log('thumbanila ', typeof this.thumbNailImagesList)
      this.getDefaultThumbNail();
      this.selectedImage = this.thumbNailImagesList[0];
      console.log('first index', this.selectedImage)
      /* to disable the next button */
      if (this.thumbNailImagesList.length > this.commonConstants.nextIndex) {
        this.rightDisabled = false;
      }

    }, (error) => {
      console.log('getThumbNailImages error :', error);
    })
  }

  sendToParent(name){
    //alert("hello");
    this.childToParent.emit(name);
  }
  // on click of thumbnail image
  clickThumbnail(item, index) {
    this.currentIndex = index;
    for (let i = 0; i < this.thumbNailImagesList.length; i++) {
      if (item.thumbnail === this.thumbNailImagesList[i].thumbnail) {
        this.selectedImage = this.thumbNailImagesList[i];
        break;
      }
    }
  }


  getDefaultThumbNail() {
    let length = this.thumbNailImagesList.length;
    if (!length) {
      return;
    }
    if (length <= this.commonConstants.end) {
      this.thumbNailSnap = this.thumbNailImagesList;
      return;
    }

    this.setThumbNailSnap();
  }


  // computing next image index
  nextImages() {
    if (this.thumbnailEndIndex + 1 < this.thumbNailImagesList.length) {
      this.thumbnailOffsetIndex = this.thumbnailEndIndex + 1;
    } else {
      return;
    }
    if (this.thumbnailEndIndex + this.commonConstants.nextIndex < this.thumbNailImagesList.length) {
      this.thumbnailEndIndex += this.commonConstants.nextIndex;
    } else {
      this.thumbnailEndIndex = (this.thumbNailImagesList.length - this.thumbnailEndIndex) + this.thumbnailEndIndex;
    }
    /* this conditions will enable or disable the previous and next buttons */
    if (this.thumbnailOffsetIndex > this.commonConstants.end) {
      this.leftDisabled = false;
    }
    if ((this.thumbNailImagesList.length - 1) == this.thumbnailEndIndex) {
      this.rightDisabled = true;
    }

    this.setThumbNailSnap();
  }

  previousImage() {
    if (this.thumbnailOffsetIndex == 0) {
      return;
    }

    if (this.thumbnailOffsetIndex) {
      this.thumbnailEndIndex = this.thumbnailOffsetIndex - 1;
      if ((this.thumbnailEndIndex - this.commonConstants.end) > 0) {
        this.thumbnailOffsetIndex = this.thumbnailEndIndex - this.commonConstants.end;
      } else {
        this.thumbnailOffsetIndex = 0;
      }
    }

    if (this.thumbnailOffsetIndex === 0) {
      this.leftDisabled = true;
    }
    this.setThumbNailSnap();
  }
  // At any time we are taking only this.commonConstants.nextIndex items in array.
  setThumbNailSnap() {
    this.thumbNailSnap = [];
      for (let i = this.thumbnailOffsetIndex; i <= this.thumbnailEndIndex; i++) {
      if (this.thumbNailImagesList[i])
        this.thumbNailSnap.push(this.thumbNailImagesList[i]);
    }
  }

}
