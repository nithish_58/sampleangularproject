import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PagesRoutingModule} from '../pages/pages-routing.module';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';



@NgModule({
  declarations: [PagesComponent, HomeComponent, HeaderComponent, ContentComponent],
  imports: [
    CommonModule, PagesRoutingModule, FormsModule,ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    AccordionModule.forRoot()
  ]
})
export class PagesModule { 
  static forRoot(): ModuleWithProviders{
    return <ModuleWithProviders>{
      ngModule: PagesModule
    }
  }
}
