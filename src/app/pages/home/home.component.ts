import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent  {
  isFirstOpen = false;
   public name = '';

   public info = '';
    //Getting value from child
  childToParent(name){
    this.info=name;
    this.isFirstOpen = true;
  }
}
