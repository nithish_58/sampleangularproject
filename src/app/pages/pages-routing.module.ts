import {PagesComponent} from './pages.component';
import {Routes, RouterModule, Router} from  '@angular/router';
import {ModuleWithProviders, NgModule} from '@angular/core';
import { HomeComponent } from './home/home.component';


const routes : Routes = [{
    path: '', component : PagesComponent, 
    children:[
        {path:'', redirectTo:'home', pathMatch:'full'},
        {path:'home', component: HomeComponent}
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule {}

