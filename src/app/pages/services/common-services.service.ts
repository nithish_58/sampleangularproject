import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CommonServicesService {

  constructor(private http: HttpClient) {

  }
  getLargeImages() {
    return this.http.get('http://localhost:4200/assets/data/templates.json');
   // return this.http.post('http://localhost:8081/largeImageList');
  }

  getThumbNailImages() {
  return this.http.get('http://localhost:4200/assets/data/extendedTemplate.json');
  // return this.http.post('http://localhost:8081/thumbnailImages'); 
}
}
